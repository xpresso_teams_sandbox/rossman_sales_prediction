"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger
import pandas as pd
import os
import math
import pickle

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="xgboost_infer_graph",
                   level=logging.INFO)


class XgboostInferGraph(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """
        self.model = None
        self.Test_data = None

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        with open(os.path.join(model_path, "Xgboost.pkl"), 'rb') as pf:
            self.model = pickle.load(pf)

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        mount_path = '/inf_data/pipelines/xgboost-training-pipeline/combined_dataset'

        self.Test_data = pd.read_csv(os.path.join(mount_path, input_request['data_file']))

        test_model_data = self.Test_data
        test_model_data = test_model_data.drop('Sales', axis=1)
        return test_model_data
        pass

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        output = self.model.predict(input_request)
        logging.info("Prediction done")
        return output
        pass

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        df = self.Test_data
        df['predicted'] = output_response
        total_prediction = df.loc[:,
                           ['Day', 'Month', 'Year', 'StoreType_high_sale', 'StoreType_low_sale', 'Sales', 'predicted']]
        total_prediction.Day = total_prediction.Day.astype('str')
        total_prediction.Month = total_prediction.Month.astype('str')
        total_prediction.Year = total_prediction.Year.astype('str')
        total_prediction['Date'] = total_prediction['Day'] + '-' + total_prediction['Month'] + '-' + total_prediction[
            'Year']
        total_prediction['Date'] = pd.to_datetime(total_prediction['Date'])
        final_df = total_prediction.sort_values(by=['Date'], ascending=True)
        d = {'1': 'January', '2': 'February', '3': 'March', '4': 'April', '5': 'May', '6': 'June', '7': 'July',
             '8': 'August', '9': 'September',
             '10': 'October', '11': 'November', '12': 'December'}
        final_df['Month'] = final_df.Month.map(d)

        final_df_high_sale = final_df.loc[final_df['StoreType_high_sale'] == 1]
        final_df_low_sale = final_df.loc[final_df['StoreType_low_sale'] == 1]

        df_high_sale = final_df_high_sale.groupby(['Date']).agg({'Sales': sum, 'predicted': sum}).reset_index()
        df_low_sale = final_df_low_sale.groupby(['Date']).agg({'Sales': sum, 'predicted': sum}).reset_index()

        df_high_sale.Date = df_high_sale.Date.astype('str')
        df_low_sale.Date = df_low_sale.Date.astype('str')

        # value_dict = {'actual': df['Sales'].values.tolist(), 'predicted':  df['predicted'].values.tolist()}
        value = {'high_sale_store': {'Date': df_high_sale['Date'].values.tolist(),
                                     'actual_sale': df_high_sale['Sales'].values.tolist(),
                                     'predicted_sale': df_high_sale['predicted'].values.tolist()},
                 'low_sale_store': {'Date': df_low_sale['Date'].values.tolist(),
                                    'actual_sale': df_low_sale['Sales'].values.tolist(),
                                    'predicted_sale': df_low_sale['predicted'].values.tolist()}
                 }

        return value
        pass


if __name__ == "__main__":
    pred = XgboostInferGraph()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
