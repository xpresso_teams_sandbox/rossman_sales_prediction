"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""

import logging
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger
import os
import math
import pickle
import pandas as pd

__author__ = "### Author ###"

logger = XprLogger("xgboost_infer", level=logging.INFO)


class XgboostInfer(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()

        """ Initialize any static data required during boot up """
        self.model = None
        self.test_data = None
        self.feature_cols = ['Store', 'DayOfWeek', 'Promo', 'Assortment', 'Promo2', 'Day',
                             'Week', 'Month', 'Year', 'isCompetition', 'new_distance', 'sale_per_customer',
                             'StateHoliday_0', 'StateHoliday_a', 'StoreType_high_sale', 'StoreType_low_sale',
                             'Sales']
        # self.target_cols = ['Sales']

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        with open(os.path.join(model_path, "Xgboost.pkl"), 'rb') as pf:
            self.model = pickle.load(pf)

    @staticmethod
    def convert_distance(df):
        return 1 / math.sqrt(df['new_distance'])

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """

        feature = pd.DataFrame()
        if isinstance(input_request, list):
            feature = pd.DataFrame([input_request], columns=self.feature_cols)
        elif isinstance(input_request, dict):
            try:
                data_to_list = [input_request[col] for col in self.feature_cols]
                feature = pd.DataFrame(data=[data_to_list],
                                       columns=self.feature_cols)
            except (ValueError, TypeError, IndexError):
                raise XprExceptions(message="Invalid data input")
        else:
            raise XprExceptions(message="Invalid data input")

        feature['new_distance'] = feature.apply(lambda row: XgboostInfer.convert_distance(row), axis=1)
        feature = feature.drop('Sales', axis=1)
        return feature

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        output = self.model.predict(input_request)
        logging.info('Prediction done')
        return output

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object

        """
        print("Print List")
        return output_response.tolist()


if __name__ == "__main__":
    pred = XgboostInfer()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
