"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from xgboost.sklearn import XGBRegressor
from sklearn import metrics
from sklearn.metrics import r2_score
import pickle
import os
from sklearn.model_selection import RandomizedSearchCV
import scipy.stats as st




__author__ = "### Author ###"

logger = XprLogger("xgboost_train",level=logging.INFO)


class XgboostTrain(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self,file_path):
        super().__init__(name="xgboost_train")
        """ Initialize all the required constansts and data her """
        self.full_train_data =pd.read_csv(file_path)
        self.output_dir = "/model/xgboost/"

    @staticmethod
    def train_test_spliting(df):
        x_features = df.iloc[:, df.columns != "Sales"]
        x_labels = df.iloc[:, df.columns == "Sales"]
        X_train, X_test, Y_train, Y_test = train_test_split(x_features, x_labels, test_size=0.2, random_state=0)
        return X_train, X_test, Y_train, Y_test

    @staticmethod
    def hyperparameter_tuning(X, Y):
        model = XGBRegressor()
        one_to_left = st.beta(10, 1)
        from_zero_positive = st.expon(0, 50)
        params = {
            "n_estimators": st.randint(3, 40),
            "max_depth": st.randint(3, 40),
            "learning_rate": st.uniform(0.05, 0.4),
            "colsample_bytree": one_to_left,
            "subsample": one_to_left,
            "gamma": st.uniform(0, 10),
            "reg_alpha": from_zero_positive,
            "min_child_weight": from_zero_positive,
        }


        grid_search = RandomizedSearchCV(estimator=model, param_distributions=params, n_jobs=1, verbose=10)
        grid_search.fit(X, Y)

        return grid_search.best_score_, grid_search.best_params_

    @staticmethod
    def final_model(X_train, Y_train):
        score, max_keys = XgboostTrain.hyperparameter_tuning(X_train, Y_train)
        best_model = XGBRegressor(colsample_bytree=max_keys['colsample_bytree'], gamma=max_keys['gamma'],
                                  learning_rate=max_keys['learning_rate'],
                                  max_depth=max_keys['max_depth'], min_child_weight=max_keys['min_child_weight'],
                                  n_estimators=max_keys['n_estimators'],
                                  reg_alpha=max_keys['reg_alpha'], subsample=max_keys['subsample'])
        best_model.fit(X_train, Y_train)
        return best_model

    @staticmethod
    def evaluation(model_obj, X_test, X_train, Y_test, Y_train):
        y_pred_test = model_obj.predict(X_test)
        y_pred_train = model_obj.predict(X_train)
        r2_score_train = r2_score(Y_train, y_pred_train)
        r2_score_test = r2_score(Y_test, y_pred_test)
        return r2_score_train, r2_score_test

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """

        saved_file_name = ""
        try:
            super().start(xpresso_run_name=run_name)
            X_train, X_test, Y_train, Y_test = XgboostTrain.train_test_spliting(self.full_train_data)
            X_train.sale_per_customer = X_train.sale_per_customer.astype('int64')
            model_obj = XgboostTrain.final_model(X_train, Y_train)
            r2_score_train, r2_score_test = XgboostTrain.evaluation(model_obj, X_test, X_train, Y_test, Y_train)
            print(r2_score_test)
            print(r2_score_train)
            self.send_metrics(desc="r2score on train data", value=r2_score_train)
            self.send_metrics(desc="r2score on test data", value=r2_score_test)
            logging.info("Model storing....")
            saved_file_name = 'Xgboost.pkl'
            if not os.path.exists(self.OUTPUT_DIR):
                os.makedirs(self.OUTPUT_DIR)
            pickle.dump(model_obj, open(os.path.join(self.OUTPUT_DIR, saved_file_name), 'wb'))

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self,desc,value):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": desc},
                "metric": {"metric_key": value}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = XgboostTrain(file_path="/data/combined_dataset/xgboost_combined_train.csv")
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")