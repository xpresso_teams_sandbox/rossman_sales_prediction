"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import pandas as pd
import numpy as np
import math
from sklearn import preprocessing
import os

__author__ = "### Author ###"

logger = XprLogger("xgboost_data_prep", level=logging.INFO)


class XgboostDataPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, train_file_path, test_file_path, store_file_path):
        super().__init__(name="xgboost_data_prep")
        """ Initialize all the required constansts and data her """
        self.train_data = pd.read_csv(train_file_path)
        self.store_data = pd.read_csv(store_file_path)
        self.test_data = pd.read_csv(test_file_path)

        # Merging Store file into a single dataset for train and test
        self.combined_train_data = None
        self.combined_test_data = None
        logging.info("Data load completed")

    @staticmethod
    def prepare_date(data):
        data['Date'] = pd.to_datetime(data['Date'], errors='coerce')
        # data['Date'] = data['Date'].dt.strftime('%d-%m-%Y')
        data['Day'] = data['Date'].dt.day
        data['Week'] = data['Date'].dt.week
        data['Month'] = data['Date'].dt.month
        data['Year'] = data['Date'].dt.year
        return data

    @staticmethod
    def is_competition(row):
        """ Use Competition open time to determine if valid competition """
        if row['Year'] >= row['CompetitionOpenSinceYear'] and \
                row['Month'] >= row['CompetitionOpenSinceMonth']:
            return 1
        else:
            return 0

    @staticmethod
    def is_promo2(row):
        """Use promo timestamp to determine if promo is active or not """
        if row['Promo2'] == 1 and \
                row['Year'] >= row['Promo2SinceYear'] and \
                row['Week'] >= row['Promo2SinceWeek']:
            return 1
        else:
            return 0

    @staticmethod
    def convert_distance(df):
        return 1 / math.sqrt(df['CompetitionDistance'])

    @staticmethod
    def convert_customer(row):
        return row['Sales'] / row['Customers']

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """

        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        self.train_data.loc[self.train_data['StateHoliday'] == 0, 'StateHoliday'] = "0"
        self.train_data = self.train_data.loc[self.train_data['Sales'] > 0]
        self.train_data['DayOfWeek'] = np.where(self.train_data['DayOfWeek'].isin([2, 3, 4, 5]), 2,
                                                self.train_data['DayOfWeek'])
        self.train_data['StateHoliday'] = np.where(self.train_data['StateHoliday'].isin(['b', 'c']), 'a',
                                                   self.train_data['StateHoliday'])
        unique_test_store = list(self.test_data.Store.unique())
        unique_train_store = list(self.train_data.Store.unique())
        self.train_data = self.train_data.loc[self.train_data['Store'].isin(unique_test_store)]

        self.store_data.set_index('Store', inplace=True)
        combined_train_data = self.train_data.join(self.store_data, on='Store')

        combined_train_data = combined_train_data.drop(['SchoolHoliday'], axis=1)

        combined_train_data = XgboostDataPrep.prepare_date(combined_train_data)

        combined_train_data[
            'CompetitionOpenSinceYear'] = combined_train_data.CompetitionOpenSinceYear.fillna(0)
        combined_train_data[
            'CompetitionOpenSinceMonth'] = combined_train_data.CompetitionOpenSinceMonth.fillna(0)

        combined_train_data.CompetitionOpenSinceYear = combined_train_data.CompetitionOpenSinceYear.astype(
            'int64')
        combined_train_data.CompetitionOpenSinceYear = combined_train_data.CompetitionOpenSinceYear.astype(
            'int64')

        combined_train_data['isCompetition'] = combined_train_data.apply(
            lambda row: XgboostDataPrep.is_competition(row), axis=1)

        combined_train_data['Promo2SinceYear'] = combined_train_data.Promo2SinceYear.fillna(0)
        combined_train_data['Promo2SinceWeek'] = combined_train_data.Promo2SinceWeek.fillna(0)

        combined_train_data.Promo2SinceYear = combined_train_data.Promo2SinceYear.astype('int64')
        combined_train_data.Promo2SinceWeek = combined_train_data.Promo2SinceWeek.astype('int64')

        combined_train_data['Promo2'] = combined_train_data.apply(lambda row: XgboostDataPrep.is_promo2(row),
                                                                  axis=1)

        combined_train_data = combined_train_data.drop(
            ['CompetitionOpenSinceMonth', 'CompetitionOpenSinceYear', 'Promo2SinceWeek', 'Promo2SinceYear'], axis=1)

        max_dist = combined_train_data['CompetitionDistance'].max()
        combined_train_data['CompetitionDistance'] = combined_train_data['CompetitionDistance'].fillna(
            max_dist)

        combined_train_data['new_distance'] = combined_train_data.apply(lambda row:
                                                                        XgboostDataPrep.convert_distance(row),
                                                                        axis=1)
        combined_train_data['sale_per_customer'] = combined_train_data.apply(lambda row:
                                                                             XgboostDataPrep.convert_customer(row),
                                                                             axis=1)
        combined_train_data = combined_train_data.drop(['Customers', 'CompetitionDistance'], axis=1)

        combined_train_data['StoreType'] = np.where(
            combined_train_data['StoreType'].isin(['a', 'c', 'd']),
            'low_sale', 'high_sale')

        combined_train_data = combined_train_data.drop('PromoInterval', axis=1)
        label_encoder = preprocessing.LabelEncoder()
        combined_train_data['Assortment'] = label_encoder.fit_transform(combined_train_data['Assortment'])
        columns = ['StateHoliday', 'StoreType']
        for col in columns:
            combined_train_data = pd.concat(
                [combined_train_data, pd.get_dummies(combined_train_data[col], prefix=col)], axis=1)
            combined_train_data.drop([col], axis=1, inplace=True)

        combined_train_data = combined_train_data.drop(['Date', 'Open'], axis=1)
        print(combined_train_data.columns)

        self.test_data['Open'] = self.test_data.Open.fillna(1)
        self.test_data['Open'] = self.test_data.Open.astype('int64')

        self.test_data = self.test_data[self.test_data['Open'] == 1]
        self.test_data['DayOfWeek'] = np.where(self.test_data['DayOfWeek'].isin([2, 3, 4, 5]), 2,
                                               self.test_data['DayOfWeek'])

        combined_test_data = self.test_data.join(self.store_data, on='Store')

        combined_test_data = XgboostDataPrep.prepare_date(combined_test_data)

        combined_test_data[
            'CompetitionOpenSinceYear'] = combined_test_data.CompetitionOpenSinceYear.fillna(0)
        combined_test_data[
            'CompetitionOpenSinceMonth'] = combined_test_data.CompetitionOpenSinceMonth.fillna(0)
        combined_test_data.CompetitionOpenSinceYear = combined_test_data.CompetitionOpenSinceYear.astype(
            'int64')
        combined_test_data.CompetitionOpenSinceYear = combined_test_data.CompetitionOpenSinceYear.astype(
            'int64')
        combined_test_data['isCompetition'] = combined_test_data.apply(lambda row: XgboostDataPrep.is_competition(row),
                                                                       axis=1)
        combined_test_data['Promo2SinceYear'] = combined_test_data.Promo2SinceYear.fillna(0)
        combined_test_data['Promo2SinceWeek'] = combined_test_data.Promo2SinceWeek.fillna(0)

        combined_test_data.Promo2SinceYear = combined_test_data.Promo2SinceYear.astype('int64')
        combined_test_data.Promo2SinceWeek = combined_test_data.Promo2SinceWeek.astype('int64')

        combined_test_data['Promo2'] = combined_test_data.apply(lambda row: XgboostDataPrep.is_promo2(row), axis=1)
        combined_test_data = combined_test_data.drop(
            ['CompetitionOpenSinceMonth', 'CompetitionOpenSinceYear', 'Promo2SinceWeek', 'Promo2SinceYear'], axis=1)

        max_dist_test = combined_test_data['CompetitionDistance'].max()
        combined_test_data['CompetitionDistance'] = combined_test_data['CompetitionDistance'].fillna(
            max_dist_test)

        combined_test_data['new_distance'] = combined_test_data.apply(lambda row:
                                                                      XgboostDataPrep.convert_distance(row), axis=1)
        combined_test_data = combined_test_data.drop(['CompetitionDistance'], axis=1)
        combined_test_data['StoreType'] = np.where(combined_test_data['StoreType'].isin(['a', 'c', 'd']),
                                                   'low_sale', 'high_sale')
        combined_test_data = combined_test_data.drop(['Date', 'Open', 'SchoolHoliday', 'PromoInterval'],
                                                     axis=1)
        combined_test_data['Assortment'] = label_encoder.fit_transform(combined_test_data['Assortment'])
        columns = ['StateHoliday', 'StoreType']
        for col in columns:
            combined_test_data = pd.concat(
                [combined_test_data, pd.get_dummies(combined_test_data[col], prefix=col)], axis=1)
            combined_test_data.drop([col], axis=1, inplace=True)

        print(combined_test_data.columns)

        self.send_metrics("Shape of the training data", combined_train_data)
        self.send_metrics("Shape of the training data", combined_test_data)
        self.combined_train_data = combined_train_data
        self.combined_test_data = combined_test_data
        logging.info("Completed data preparation")

        self.completed()

    def send_metrics(self, desc, data):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": desc},
            "metric": {"rows": str(data.shape[0]),
                       "columns": str(data.shape[1])}
        }

        self.report_status(status=report_status)

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        output_dir = "/data/combined_dataset/"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        self.combined_train_data.to_csv(os.path.join(output_dir,
                                                     "xgboost_combined_train.csv"),
                                        index=False)
        self.combined_test_data.to_csv(
            os.path.join(output_dir, "xgboost_combined_test.csv"),
            index=False)
        logging.info("Data saved")
        super().completed(push_exp=False)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = XgboostDataPrep(train_file_path="/data/combined_dataset/train.csv",
                                test_file_path="/data/combined_dataset/test.csv",
                                store_file_path="/data/combined_dataset/store.csv")
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
